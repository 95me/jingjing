package com.jingjing.scheduled;

import com.jingjing.service.user.LoginUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @program: jingjing
 * @description: 用户相关任务
 * @author: 何伟
 * @create: 2019-03-20 18:54
 **/
@Component
public class UserTask {

    private static Logger logger= LoggerFactory.getLogger(UserTask.class);

    @Value("${server.servlet.session.timeout}")
    private int timeout;
    @Autowired
    LoginUserService loginUserService;

    /**
     * 从数据库删除会话过期的用户
     * 定时执行，每隔1分钟
     */
    @Async
    @Scheduled(fixedRate = 60*1000)
    public void removeTimeOutUser(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.SECOND,-timeout);
        Date loginTime=calendar.getTime();


        loginUserService.removeTimeOutUser(loginTime);

        logger.info("在  "+sdf.format(loginTime)+"   时刻以前登录的用户会话已超时！");
    }


}
