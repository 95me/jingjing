package com.jingjing.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @program: jingjing
 * @description: 判断用户是否登录
 * @author: 何伟
 * @create: 2019-03-19 11:41
 **/

//@WebFilter(filterName = "LoginFilter",urlPatterns = "/*")
public class LoginFilter implements Filter {
    private Logger logger= LoggerFactory.getLogger(this.getClass());

    @Override
    public void init(FilterConfig fc) throws ServletException{
        logger.info("过滤器-"+fc.getFilterName()+"-初始化");
    }

    @Override
    public void doFilter(ServletRequest request,ServletResponse response,FilterChain chain) throws IOException,ServletException{

    }

    @Override
    public void destroy() {

    }
}
