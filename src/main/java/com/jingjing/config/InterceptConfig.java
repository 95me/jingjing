package com.jingjing.config;

import com.jingjing.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @program: jingjing
 * @description: 拦截器配置
 * @author: 何伟
 * @create: 2019-03-19 15:03
 **/
@Configuration
public class InterceptConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;
    private final String[] notLoginInterceptPaths = {
            "/regist","/login","/logout","/templates/**","/resources/**", "/static/**","/public/**"
            ,"/doLogin","/doRegist","/error","/jsp/**"};

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry
                .addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(notLoginInterceptPaths);
//        super.addInterceptors(registry);
    }
}
