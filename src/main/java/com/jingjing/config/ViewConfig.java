package com.jingjing.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * @program: jingjing
 * @description: 模板引擎视图配置
 * @author: 何伟
 * @create: 2019-03-25 15:29
 **/

//多视图解析配置失败
//@Configuration
//@EnableWebMvc
//@ComponentScan("com.jingjing.*")
public class ViewConfig /*extends WebMvcConfigurationSupport */{
/*
    @Value("${spring.mvc.view.prefix}")
    private String jspPrefix;
    @Value("${spring.mvc.view.suffix}")
    private String jspSuffix;
    @Value("${spring.mvc.view.order}")
    private int jspOrder;
    @Value("${spring.mvc.view.view-name}")
    private String jspViewNnames;

    @Value("${spring.thymeleaf.prefix}")
    private String thPrefix;
    @Value("${spring.thymeleaf.suffix}")
    private String thSuffix;
    @Value("${spring.thymeleaf.template-resolver-order}")
    private int thOrder;
    @Value("${spring.thymeleaf.cache}")
    private boolean cacheable;
//    @Value("${spring.mvc.view.view-name}")
//    private String thViewNnames;

//  jsp页面的视图解析器
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix(jspPrefix);
        resolver.setSuffix(jspSuffix);
        resolver.setViewNames(jspViewNnames);
        resolver.setOrder(jspOrder);
        return resolver;
    }


//    Thymeleaf视图解析器
    @Bean
    public ITemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setTemplateMode("HTML5");
        templateResolver.setPrefix(thPrefix);
        templateResolver.setSuffix(thSuffix);
        templateResolver.setCharacterEncoding("utf-8");
        templateResolver.setCacheable(cacheable);
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        // templateEngine
        return templateEngine;
    }

    @Bean
    public ThymeleafViewResolver viewResolverThymeLeaf() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setCharacterEncoding("utf-8");
        viewResolver.setOrder(thOrder);
        viewResolver.setViewNames(new String[]{"/*"});
        return viewResolver;
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
    }*/
}
