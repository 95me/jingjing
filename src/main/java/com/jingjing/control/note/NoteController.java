package com.jingjing.control.note;

import com.jingjing.model.note.Note;
import com.jingjing.model.note.NoteBook;
import com.jingjing.service.note.NoteBookService;
import com.jingjing.service.note.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: jingjing
 * @description: 笔记相关
 * @author: 何伟
 * @create: 2019-04-17 16:45
 **/
@Controller
public class NoteController {

    @Autowired
    NoteService noteService;
    @Autowired
    NoteBookService bookService;

    Logger logger= LoggerFactory.getLogger(this.getClass());

    @RequestMapping("notelist")
    public ModelAndView noteList(HttpSession session, Long bookId){

        NoteBook book=null;
        List<Note> notes=new ArrayList<Note>();
        ModelAndView mv=new ModelAndView();
        try{
            book=bookService.getBook(bookId);
            notes=noteService.getBookNotes(bookId);
        }catch (Exception e){
            mv.setViewName("jingjing-error");
            mv.addObject("ErrorCode","500");
            mv.addObject("ErrorTitle","获取笔记列表出错");
            mv.addObject("ErrorMsg",e.getMessage());
            return mv;
        }

        mv.setViewName("note-list-img");
        mv.addObject("book",book);
        mv.addObject("notes",notes);
        return mv;
    }

    @RequestMapping("removenote")
    public String removeNote(ModelMap modelMap,Long noteid,Long bookid){

        try{
            noteService.delNoteById(noteid);
        }catch (Exception e){
            logger.error("删除笔记出错："+e.getMessage());
            modelMap.addAttribute("ErrorCode","500");
            modelMap.addAttribute("ErrorTitle","服务器异常");
            modelMap.addAttribute("ErrorCode","删除笔记失败！");
            return "jingjing-error.html";
        }
        return "redirect:notelist?bookId="+bookid;
    }

    @RequestMapping("notecontent")
    public String noteContent(ModelMap modelMap,Long noteid){
        Note note=null;
        try{
            note=noteService.getNoteById(noteid);
            modelMap.addAttribute("note",note);
        }catch (Exception e){
            logger.error("查询笔记出错："+e.getMessage());
            modelMap.addAttribute("ErrorCode","500");
            modelMap.addAttribute("ErrorTitle","服务器异常");
            modelMap.addAttribute("ErrorCode","查询笔记失败！");
            return "jingjing-error.html";
        }
        return "note-content";
    }

    @RequestMapping("searchNotes")
    public ModelAndView searchNotes(HttpSession session, String title){

        List<Note> notes=new ArrayList<Note>();
        ModelAndView mv=new ModelAndView();
        try{
            notes=noteService.getNotes(title);
        }catch (Exception e){
            mv.setViewName("jingjing-error");
            mv.addObject("ErrorCode","500");
            mv.addObject("ErrorTitle","获取笔记列表出错");
            mv.addObject("ErrorMsg",e.getMessage());
            return mv;
        }

        mv.setViewName("note-list-search");
        mv.addObject("notes",notes);
        return mv;
    }
}
