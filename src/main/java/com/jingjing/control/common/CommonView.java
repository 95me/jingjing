package com.jingjing.control.common;

import com.jingjing.model.note.NoteBook;
import com.jingjing.model.user.User;
import com.jingjing.service.note.NoteBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 公共的跳转页面
 */
@Controller
public class CommonView {

    @Autowired
    private NoteBookService noteBookService;

    @RequestMapping("/")
    public String defaultView(HttpSession session, ModelMap modelMap){
        if (null!=session) {
            User user=(User)session.getAttribute("user");
            if (user==null){
                modelMap.addAttribute("ErrorCode","500");
                modelMap.addAttribute("ErrorTitle","登录异常");
                modelMap.addAttribute("ErrorMsg","用户登录异常，会话无用户信息！");
                return "jingjing-error";
            }
            List<NoteBook> noteBooks=noteBookService.getUserNoteBooks(user.getId());
            modelMap.addAttribute("user",user);
            modelMap.addAttribute("notebooks",noteBooks);
        }
        return "index";
    }
    @RequestMapping("login")
    public String login(){
        return "login";
    }
    @RequestMapping("regist")
    public String regist(){
        return "sign-up";
    }

    @RequestMapping("indexContent")
    public String indexContent(){
        return "index-content";
    }

    @RequestMapping("addNoteBookPage")
    public String addNoteBookPage(){
        return "add-notebook";
    }



}
