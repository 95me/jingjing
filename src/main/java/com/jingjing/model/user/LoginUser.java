package com.jingjing.model.user;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * @program: jingjing
 * @description: 登录用户
 * @author: 何伟
 * @create: 2019-03-20 11:09
 **/
@Entity
public class LoginUser {
    @Id
    private long id;
    private long userId;
    private Date loginTime;
    private String ip;
    private int port;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
