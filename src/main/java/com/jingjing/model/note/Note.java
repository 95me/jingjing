package com.jingjing.model.note;

import java.util.Date;
import java.sql.Clob;

public class Note {
    private Long id;

    private Long notebookId;

    private String title;

    private String content;

    private String imgcover;

    private Long shareUserId;

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNotebookId() {
        return notebookId;
    }

    public void setNotebookId(Long notebookId) {
        this.notebookId = notebookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgcover() {
        return imgcover;
    }

    public void setImgcover(String imgcover) {
        this.imgcover = imgcover == null ? null : imgcover.trim();
    }

    public Long getShareUserId() {
        return shareUserId;
    }

    public void setShareUserId(Long shareUserId) {
        this.shareUserId = shareUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}