package com.jingjing.service.user;

import com.jingjing.mapper.LoginUserMapper;
import com.jingjing.model.user.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @program: jingjing
 * @description: 登录用户管理
 * @author: 何伟
 * @create: 2019-03-20 11:48
 **/

@Service()
public class LoginUserService {

    @Autowired
    LoginUserMapper loginUserMapper;

    public List<LoginUser> getLoginUser(Long uid){
        return loginUserMapper.selectByUserId(uid);
    }

    public int addLoginUser(LoginUser user){
        List<LoginUser> u=getLoginUser(user.getUserId());
        if (u.size()<2){
            return loginUserMapper.insert(user);
        }else{
            return -1;
        }
    }

    public int removeLoginUser(Long uid, Date date){
        return loginUserMapper.deleteCurrentUser(uid, date);
    }

    public int removeTimeOutUser(Date loginTime){
        return loginUserMapper.removeTimeOutUser(loginTime);
    }

    public List<LoginUser> allLoginUser(){
        return loginUserMapper.allLoginUser();
    }
}
