package com.jingjing.service.user;

import com.jingjing.mapper.UserMapper;
import com.jingjing.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: jingjing
 * @description: 账户相关逻辑
 * @author: 何伟
 * @create: 2019-03-13 10:55
 **/
@Service("userService")
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public int addUser(User user){
        String name=user.getName();
        User u=userMapper.getUserByName(name);
        if (null==u){
            return userMapper.insert(user);
        }else{
            return -1;
        }
    }

    public User getUser(Long id){
        return userMapper.selectByPrimaryKey(id);
    }

    public User getUserByName(String name){
        return userMapper.getUserByName(name);
    }

    public int updateUser(User user){
        return userMapper.updateByPrimaryKey(user);
    }

}
