package com.jingjing.service.note;

import com.jingjing.mapper.NoteBookMapper;
import com.jingjing.model.note.NoteBook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: jingjing
 * @description: 笔记本服务
 * @author: 何伟
 * @create: 2019-04-10 16:52
 **/
@Service
public class NoteBookService {

    Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    private NoteBookMapper noteBookMapper;

    public List<NoteBook> getUserNoteBooks(Long userId){
        if (null!=userId){
            return noteBookMapper.selectByUserId(userId);
        }else {
            return new ArrayList<NoteBook>();
        }
    }

    public void addNoteBook(NoteBook noteBook){
        if (noteBook!=null){
            noteBookMapper.insert(noteBook);
        }
    }

    public int deleteNoteBook(Long bookId){
        int result=0;
        try {
            result = noteBookMapper.deleteByPrimaryKey(bookId);
        }catch (Exception e){
            result=-1;
            logger.info("删除笔记本出错，错误信息："+e.getMessage());
        }
        return result;
    }

    public NoteBook getBook(Long bookid){
        NoteBook book=noteBookMapper.selectByPrimaryKey(bookid);

        return book;
    }

}
