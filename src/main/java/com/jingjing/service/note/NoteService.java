package com.jingjing.service.note;

import com.jingjing.mapper.NoteMapper;
import com.jingjing.model.note.Note;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: jingjing
 * @description: 笔记相关业务
 * @author: 何伟
 * @create: 2019-04-16 18:17
 **/

@Service
public class NoteService {

    private Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    NoteMapper noteMapper;

    public List<Note> getBookNotes(Long bookId){
        List<Note> bookNotes=new ArrayList<Note>();
        if (null!=bookId){
            bookNotes=noteMapper.selectByBookId(bookId);
        }
        return bookNotes;
    }

    public int delNoteById(Long bookid){
        return noteMapper.deleteByPrimaryKey(bookid);
    }

    public int saveNote(Note note){
        int count=0;
        try{
            count=noteMapper.insert(note);
        }catch (Exception e){

        }
        return count;
    }

    public int updateNote(Note note){
        return noteMapper.updateByPrimaryKey(note);
    }

    public int updateContent(Long noteid,String content){
        return noteMapper.updateContentById(noteid,content);
    }
    public int updateImgcover(Long noteid,String imgcover){
        return noteMapper.updateImgcoverById(noteid,imgcover);
    }

    public Note getNoteById(Long id){
        return noteMapper.selectByPrimaryKey(id);
    }

    public List<Note> getNotes(String title){
        return noteMapper.selectByTitle(title);
    }
}
