package com.jingjing.service.common;

import com.jingjing.model.note.Note;
import com.jingjing.model.user.User;
import com.jingjing.service.note.NoteBookService;
import com.jingjing.service.note.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.Date;

/**
 * @program: jingjing
 * @description: 百度编辑器服务类
 * @author: 何伟
 * @create: 2019-04-28 17:39
 **/
@Service
public class UeditorService {
    @Autowired
    NoteService noteService;

    Logger logger= LoggerFactory.getLogger(this.getClass());


}
