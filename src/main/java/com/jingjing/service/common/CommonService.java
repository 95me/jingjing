package com.jingjing.service.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: jingjing
 * @description: 公共服务
 * @author: 何伟
 * @create: 2019-03-25 10:13
 **/

@Service()
@Scope(value = "session",proxyMode= ScopedProxyMode.TARGET_CLASS)
public class CommonService {
    private Logger logger= LoggerFactory.getLogger(this.getClass());
    /**
     *
     * getRemoteIP:获取远程请求客户端的外网IP <br/>
     *
     * @param request
     *            请求实体对象
     * @return ip 外网ip<br/>
     */
    public String getRemoteIP(HttpServletRequest request) {
        String remoteHost=request.getRemoteHost();
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//            ip = request.getRemoteAddr();
            ip=remoteHost;
        }

        logger.info("远程客户端ip："+request.getRemoteAddr());//取得客户端的IP
        logger.info("远程客户端主机名："+request.getRemoteHost());//取得客户端的主机名
        logger.info("远程客户端端口："+request.getRemotePort()+"");//取得客户端的端口
        logger.info("远程客户端用户："+request.getRemoteUser());
        return ip;
    }
}
