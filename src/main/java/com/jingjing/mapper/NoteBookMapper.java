package com.jingjing.mapper;

import com.jingjing.model.note.NoteBook;

import java.util.List;

public interface NoteBookMapper {
    int deleteByPrimaryKey(Long id);

    int insert(NoteBook record);

    int insertSelective(NoteBook record);

    NoteBook selectByPrimaryKey(Long id);
    List<NoteBook> selectByUserId(Long userId);

    int updateByPrimaryKeySelective(NoteBook record);

    int updateByPrimaryKey(NoteBook record);
    int updateByUserId(NoteBook record);
}