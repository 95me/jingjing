package com.jingjing.mapper;

import com.jingjing.model.note.Note;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoteMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Note record);

    int insertSelective(Note record);

    Note selectByPrimaryKey(Long id);

    List<Note> selectByBookId(Long bookId);

    List<Note> selectByTitle(String title);

    int updateByPrimaryKeySelective(Note record);

    int updateByPrimaryKey(Note record);

    int updateContentById(@Param("id") Long id, @Param("content") String content);

    int updateImgcoverById(@Param("id") Long id, @Param("imgcover") String imgcover);

}