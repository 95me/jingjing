package com.jingjing.mapper;

import com.jingjing.model.UserModule;

public interface UserModuleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(UserModule record);

    int insertSelective(UserModule record);

    UserModule selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserModule record);

    int updateByPrimaryKey(UserModule record);
}