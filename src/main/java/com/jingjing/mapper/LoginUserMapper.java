package com.jingjing.mapper;

import com.jingjing.model.user.LoginUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @program: jingjing
 * @description:
 * @author: hewei
 * @create: 2019-03-20 11:17
 **/

public interface LoginUserMapper {

    int deleteCurrentUser(@Param("uid") Long uid, @Param("loginTime")Date loginTime);

    int insert(LoginUser record);

    List<LoginUser> selectByUserId(Long uid);

    int updateByUserId(LoginUser record);

    int removeTimeOutUser(Date loginTime);

    List<LoginUser> allLoginUser();

}
