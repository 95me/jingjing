$(function () {
    $("table").wrap('<div class=\"am-scrollable-horizontal \"></div>');

    $("table").addClass("am-table am-table-bordered am-text-nowrap");

    SyntaxHighlighter.config.clipboardSwf = 'scripts/clipboard.swf';

    SyntaxHighlighter.config.strings = {

        expandSource : '展开代码',

        viewSource : '查看代码',

        copyToClipboard : '复制代码',

        copyToClipboardConfirmation : '代码复制成功',

        print : '打印',

        help: '?',

        alert: '语法高亮\n\n',

        noBrush: '不能找到刷子: ',

        brushNotHtmlScript: '刷子没有配置html-script选项',

        aboutDialog: '<div></div>'

    };

    //巨坑：防止全局变量初始化一次之后影响其他页面显示，所以每次都清空
    SyntaxHighlighter.vars.discoveredBrushes=null;

    //动态加载js
    $("pre").each(function (index,element) {
        var str=$(this).attr("class").split(";")[0];
        var lange=str.split(":")[1];
        var url=langePath+langeData[lange];
        $.ajax({
            url: url,
            dataType: "script",
            async:false,
            cache: true,
            success: function () {
                console.log(url+" 加载完毕！")
            }
        });
    });

    SyntaxHighlighter.highlight();
    // SyntaxHighlighter.all();

    /*
    debugger;
    function path(){
        var args = arguments,
            result = [];
        for(var i = 0; i < args.length; i++)
            result.push(args[i].replace('@', '/static/syntaxhighlighter_3.0.83/scripts/'));//请替换成自己项目中SyntaxHighlighter的具体路径
        return result
    };
    SyntaxHighlighter.autoloader.apply(null, path(
        'applescript        @shBrushAppleScript.js',
        'actionscript3 as3      @shBrushAS3.js',
        'bash shell     @shBrushBash.js',
        'coldfusion cf      @shBrushColdFusion.js',
        'cpp c          @shBrushCpp.js',
        'c# c-sharp csharp      @shBrushCSharp.js',
        'css            @shBrushCss.js',
        'delphi pascal      @shBrushDelphi.js',
        'diff patch pas     @shBrushDiff.js',
        'erl erlang     @shBrushErlang.js',
        'groovy         @shBrushGroovy.js',
        'java           @shBrushJava.js',
        'jfx javafx     @shBrushJavaFX.js',
        'js jscript javascript  @shBrushJScript.js',
        'perl pl            @shBrushPerl.js',
        'php            @shBrushPhp.js',
        'text plain     @shBrushPlain.js',
        'py python          @shBrushPython.js',
        'ruby rails ror rb      @shBrushRuby.js',
        'sass scss          @shBrushSass.js',
        'scala          @shBrushScala.js',
        'sql            @shBrushSql.js',
        'vb vbnet           @shBrushVb.js',
        'xml xhtml xslt html        @shBrushXml.js'
    ));*/

    $("[data-note-edit]").on('click', function (event) {
        debugger;
        //阻止默认时间
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();

        var noteTitle=$("#noteid").text();
        var noteid = $(this).attr("data-note-edit");

        localStorage.setItem("notetitle", noteTitle);
        localStorage.setItem("noteid", noteid);
        localStorage.setItem("pageType","edit");
        layer.open({
            id: 'ueditor-edit',
            type: 2,
            title: "编辑 "+noteTitle,
            shadeClose: false,
            shade: true,
            maxmin: true, //开启最大化最小化按钮
            area: ['893px', '600px'],
            // content: '//fly.layui.com/'
            content: '/ueditoredit?noteid='+noteid,
            cancel: function(){
                $.get("/notecontent", {noteid: noteid}, function (result) {
                    $("#content-box").html(result);
                });
            }
        });
    });
})