$(function () {
    $(".note-search-button").click(function () {

        //阻止事件冒泡
        event.stopPropagation();
        //阻止默认事件
        event.preventDefault();
        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            var  search_item=$('#note_title').val();
            $.get("/searchNotes", {title: search_item}, function (result) {
                $("#content-box").html(result);
            });
        }
    });
})