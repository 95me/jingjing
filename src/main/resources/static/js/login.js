$(document).ready(function () {
    //是否跳转到主页面
    var toMain=false;

    $.validator.setDefaults({
        //绑定的是submit事件
        submitHandler: function() {
            console.log("submit事件触发");
            //验证通过后登录
            if (toMain){
                login();
            }
        }
    });

    $('#loginForm').validate({
        rules: {
            name:{required:true},
            pwd:{required:true}
        },
        messages: {
        }
    });
    debugger;

    $('#mpanel').codeVerify({
        type : 1,
        width : '300px',
        height : '35px',
        fontSize : '30px',
        codeLength : 4,
        //绑定的是clic事件
        btnId : 'loginBtn',
        ready : function() {
        },
        success : function() {
            console.log("验证码校验成功！");
            toMain= true;
        },
        error : function() {
            console.log("验证码校验失败！");
            layer.alert('验证码不匹配！',{icon:2});
            toMain= false;
        }
    });

    $('input[class="varify-input-code"]').attr('placeholder',"请输入验证码");

    function login() {
        var pwd=$('#pwd').val();
        pwd=CryptoJS.SHA1(pwd).toString();
        var data={
            name:$('#name').val(),
            pwd:pwd
        };
        console.log(JSON.stringify(data));
        $.ajax({
            type:'POST',
            url:'/doLogin',
            //以表单的形式发送，后台可以多参数接收
            // contentType:'application/x-www-form-urlencoded',
            // contentType:'application/json',
            //接收后的数据是Json格式
            dataType:'json',
            // data:JSON.stringify(data),
            data:data,
            success:function(result){
                debugger;
                // result=JSON.parse(result);
                console.log(result);
                var errmsg="";
                if (result.login=="faild") {
                    delete result.login;
                    for (var k in result){
                        errmsg+=k+":"+result[k]+"</br>";
                    }
                    layer.alert(errmsg,{icon:2});
                }else if (result.login=="success") {
                    debugger;
                    var user={};
                    user.username=result.username;
                    user.logintime=result.logintime;
                    user.timeout=result.timeout;
                    localStorage.setItem("user",user);
                    console.log("用户："+user.username+" 于 "+user.logintime+" 时刻登录。");
                    location.href='/';
                }
            },
            error:function(result) {
                layer.msg("发生异常，请稍后再试！",{icon:2,time:1000});
                console.log(result);
            }
        });
    }
})