$(function () {
    var bookid = $("#bookid").attr("data");

    $(".delnote").on("click", function (event) {
        //阻止默认事件
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();

        var todel = confirm('您确定要删除笔记吗？删除后不可恢复！');
        if (todel) {
            if (window.checkSession() == false) {
                window.location.href = "/";
            } else {
                var noteid = $(this).attr("data-noteid");
                var bookid = $(this).attr("data-bookid");
                var href = $(this).attr("href");
                $.get(href, {bookid: bookid, noteid: noteid}, function (result) {
                    $("#content-box").html(result);
                });
            }
        }
    });
    $(".looknote").on("click", function (event) {
        //阻止默认事件
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();


        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            // var content = "<div class='widget am-cf' >"+$(this).attr("data-content")+"</div>";
            // $("#content-box").html(content);
            var noteid = $(this).attr("data-noteid");
            var href = $(this).attr("href");
            $.get(href, {noteid: noteid}, function (result) {
                $("#content-box").html(result);
            });
        }
    });

    /*****************************图片上传**********************/

    $('.uploadimg').on('click', function(e) {
        e.preventDefault();
        var noteidImg=$(this).children("i").attr("data-noteid");
        console.log(noteidImg);
        localStorage.setItem("noteidImg",noteidImg);
        $('#tpfile').trigger('click');
    })

    $("#tpfile").on("change", upload );
    function upload(){
        var noteidImg=localStorage.getItem("noteidImg");
        var self = this;
        var formdata=new FormData();
        formdata.append("file",$("#formTag")[0])
        formdata.append("noteid",noteidImg);

        $.ajax({
            url: "/common/uploadImg",
            type: "post",
            dataType: "json",
            cache: false,
            data: formdata,
            processData: false,// 不处理数据
            contentType: false, // 不设置内容类型
            success: function(data){
                debugger;
                var imgobj=$("img[data-noteid="+noteidImg+"]");
                console.log(imgobj);
                imgobj.attr("src", data.path);
                /*
                图片显示路径出错，没解决：反斜杠转义
                $(self).parent().css({
                     "background-image": "url("+data.path+")"
                 })*/;
            },
            error:function (data) {
                layer.msg(data.errorMsg);
            }
        })
    }

    $("[name='tobooklist']").on("click", function (event) {
        //阻止默认事件
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();

        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            var href = $(this).attr("href");
            $.get(href, function (result) {
                $("#content-box").html(result);
            });
        }
    });
    $("[name='tonotelist']").on("click", function (event) {
        //阻止默认事件
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();

        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            var href = $(this).attr("href");
            var bookid = $(this).attr("data-bookid");
            $.get(href,{bookId: bookid}, function (result) {
                $("#content-box").html(result);
            });
        }
    });

    $("[data-note-add]").on('click', function (event) {
        debugger;
        //阻止默认时间
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();

        var noteTitle = "";
        layer.prompt({title: '输入笔记名称', formType: 0}, function (title, index) {
            noteTitle = title;
            localStorage.setItem("notetitle", noteTitle);
            localStorage.setItem("bookid", bookid);
            localStorage.setItem("pageType","add");
            layer.close(index);
            layer.open({
                id: 'ueditor',
                type: 2,
                title: "新建 "+noteTitle,
                shadeClose: false,
                shade: true,
                maxmin: true, //开启最大化最小化按钮
                area: ['893px', '600px'],
                // content: '//fly.layui.com/'
                content: '/ueditorpage'
            });
        });
    });
})