$(function () {
    // 初始化加载内容框
    $.get('/notebooks', function (result) {
        $("#content-box").html(result);
    });
    //     // 判断用户是否已有自己选择的模板风格
    //    if(storageLoad('SelcetColor')){
    //      $('body').attr('class',storageLoad('SelcetColor').Color)
    //    }else{
    //        storageSave(saveSelectColor);
    //        $('body').attr('class','theme-black')
    //    }

    autoLeftNav();
    $(window).resize(function () {
        autoLeftNav();
        console.log($(window).width())
    });

    //    if(storageLoad('SelcetColor')){

    //     }else{
    //       storageSave(saveSelectColor);
    //     }

    /***********************************************************井井记事*******************************************************/

    //前端判断用户登录是否过期
    window.checkSession = function () {
        var timeout = false;
        $.ajax({
            type: 'post',
            url: '/checkSession',
            async: false,
            //携带用户身份信息（相关的cookie）
            xhrFields: {
                withCredentials: true
            },
            //接收后的数据是Json格式
            dataType: 'json',
            data: {},
            success: function (result) {
                debugger;
                // result=JSON.parse(result);
                console.log(result);
                timeout = result.timeout;
            },
            error: function (result) {
                layer.msg("登录超时，请重新登录！", {icon: 2, time: 1000});
                console.log(result);
            }
        });

        return timeout;
    }

    //用户中心
    $('#userUnit').on('click', function (event) {
        event.preventDefault();
        //如果session过期定向到主页面
        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            $("#content-box").load($(this).attr('href'), function (result) {
                //页面加载之后加载页面中的js和css
                // result = $(result);
                // result.find("script").appendTo('#content-box');
                // result.find("link").appendTo('#content-box');
            });
        }
    })

    //左侧菜单栏显示隐藏
    $('.tpl-header-switch-button').on('click', function () {
        if ($('.left-sidebar').is('.active')) {
            //窗口宽度>1024时内容区和左侧菜单栏宽度之和等于窗口宽度
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').removeClass('active');
            }
            $('.left-sidebar').removeClass('active');
        } else {

            $('.left-sidebar').addClass('active');
            if ($(window).width() > 1024) {
                $('.tpl-content-wrapper').addClass('active');
            }
        }
    })

    //左侧菜单点击，切换内容框
    $("[data-menu-item]").on('click', function (event) {
        // debugger;
        //阻止默认事件
        event.preventDefault();
        //阻止事件冒泡
        event.stopPropagation();
        $("[data-menu-item] > a").removeClass("active");
        $(this).children('a').addClass("active");
        console.log($(this).children('a'));
        var href = $(this).children('a').attr("href");
        if (null != href || "" != href) {
            switchContent($(this).children('a').attr("href"));
        }

    })

    //
    $("#createNoteBook").on("click", function (e) {
        e.preventDefault();

        /*layer.open({
            type: 1,
            title:'添加笔记本',
            skin: 'layui-layer-rim', //加上边框
            shade: 0.8,
            area: ['420px', '240px'], //宽高
            // content: '/addNoteBookPage'
            content: static/templates/add-notebook.html
        });*/
        layer.open({
            type: 2,
            title: '添加笔记本',
            // shadeClose: true,
            shade: 0.8,
            area: ['300px', '250px'],
            content: '/addNoteBookPage' //iframe的url
        });
    });

    function switchContent(href) {
        // $("#content-box").load(href);
        if (window.checkSession() == false) {
            window.location.href = "/";
        } else {
            $.get(href, function (result) {
                $("#content-box").html(result);
            });
        }
    }

})
/***********************************************************井井记事*******************************************************/


// 风格切换

$('.tpl-skiner-toggle').on('click', function () {
    $('.tpl-skiner').toggleClass('active');
})

$('.tpl-skiner-content-bar').find('span').on('click', function () {
    $('body').attr('class', $(this).attr('data-color'))
    saveSelectColor.Color = $(this).attr('data-color');
    // 保存选择项
    storageSave(saveSelectColor);

})


// 侧边菜单开关
function autoLeftNav() {
    if ($(window).width() < 1024) {
        //左侧导航栏左移隐藏
        $('.left-sidebar').addClass('active');
    } else {
        $('.left-sidebar').removeClass('active');
    }
}


// 侧边菜单
$('.sidebar-nav-sub-title').on('click', function () {
    $(this).siblings('.sidebar-nav-sub').slideToggle(80)
        .end()
        .find('.sidebar-nav-sub-ico').toggleClass('sidebar-nav-sub-ico-rotate');
})